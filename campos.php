<?php
include "conectasql.php";
session_start();

$sql = "SELECT * FROM aluno_sem_iipcnet WHERE codigo_iipcnet IS NULL and rg IS NOT NULL"; 
$res_alunos = $conexao ->query($sql);
$aluno = $res_alunos -> fetch_assoc();


?>

<script src="jquery-3.3.1.min.js" crossorigin="anonymous"></script>

<script>

  function copy(id_element) {
    var copyText = document.getElementById(id_element);
    copyText.select();
    document.execCommand("copy");
  }

$(document).ready(function () {                         
  $('form').on('submit', function (e) {

    e.preventDefault(); //prevent to reload the page

    $.ajax({
      type: 'POST', //hide url
      url: 'enviaAluno.php', //your form validation url
      data: $('form').serialize(),//+"&id_aluno="+aluno['id'],
      success: function () {
        alert('Enviado com sucesso!'); //display an alert whether the form is submitted okay
        //location.reload();
      }
    });
  });
});
</script>

<form>
  <div class="card" style="position:static !important;margin-right:40px !important;margin-bottom:40px; text-align:center">
    <div class="card-header" >Informe o Código IIPC Net gerado para o aluno
    </div>
      <div class="card-body align-content-center row">  
        <input type="text" class="form-control" id="cod_iipcnet" name="cod_iipcnet" style="width:50%">
        <button class="btn btn-primary btn-md" style="margin-left:2px" type="submit">Enviar</button>
    </div> 
  </div>

 <div class="row form-inline mb-2" style="width:100%">
      <label for="nome">Nome</label>
      <input type="text" value="<?=utf8_encode($aluno['nome'])?>" style="margin-left:15px;width:80%;" class="form-control" id="nome" name="nome" readonly>
      <input type="button" onclick="copy('nome')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
   </div>
    <div class="row form-inline mb-2">
      <label for="rg">RG</label>
      <input type="text" value="<?=utf8_encode($aluno['rg'])?>" style="margin-left:15px;width:145px" class="form-control" id="rg" name="rg" readonly>
      <input type="button" onclick="copy('rg')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
      <label for="cpf" style="margin-left:15px">CPF</label>
      <input type="text" value="<?=utf8_encode($aluno['cpf'])?>" style="margin-left:15px" class="form-control" id="cpf" name="cpf" readonly>
      <input type="button" onclick="copy('cpf')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
    </div>
    <div class="row form-inline mb-2">
      <label for="datanasc">Data Nasc.</label>
      <input type="text" value="<?php $time = strtotime($aluno['data_nascimento']); $newformat = date('d/m/Y',$time); echo $newformat;?>" style="margin-left:15px;width:130px;" class="form-control" id="datanasc" name="datanasc" readonly>
      <input type="button" onclick="copy('datanasc')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
      <label for="sexo" style="margin-left:15px">Sexo</label>
      <input type="text" value="<?=utf8_encode($aluno['genero'])?>" style="margin-left:15px" class="form-control" id="sexo" name="sexo" readonly>
      <input type="button" onclick="copy('sexo')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
    </div>
    <div class="row form-inline mb-2">
      <label for="bairro">Bairro</label>
      <input type="text" value="<?=utf8_encode($aluno['bairro'])?>" style="margin-left:15px" class="form-control" id="bairro" name="bairro" readonly>
      <input type="button" onclick="copy('bairro')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
      <label for="uf" style="margin-left:15px;">UF</label>
      <input type="text" value="<?=utf8_encode($aluno['uf'])?>" style="margin-left:15px;width:50px;" class="form-control" id="uf" name="uf" readonly>
      <input type="button" onclick="copy('uf')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
     </div>
    <div class="row form-inline mb-2">
      <label for="cidade">Cidade</label>
      <input type="text" value="<?=utf8_encode($aluno['cidade'])?>" style="margin-left:15px" class="form-control" id="cidade" name="cidade" readonly>
      <input type="button" onclick="copy('cidade')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
    </div>  
    <div class="row form-inline mb-2">
      <label for="fixo">Tel Fixo</label>
      <input type="text" value="<?=utf8_encode($aluno['telefone_fixo'])?>" style="margin-left:15px;width:140px;" class="form-control" id="fixo" name="fixo" readonly>
      <input type="button" onclick="copy('fixo')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
      <label for="celular" style="margin-left:15px">Celular</label>
      <input type="text" value="<?=utf8_encode($aluno['celular'])?>" style="margin-left:15px" class="form-control" id="celular" name="celular" readonly>
      <input type="button" onclick="copy('celular')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
    </div>
    <div class="row form-inline mb-2">
      <label for="whatsapp">Whatsapp</label>
      <input type="text" value="<?=utf8_encode($aluno['whatsapp'])?>" style="margin-left:15px;width:130px;" class="form-control" id="whatsapp" name="whatsapp" readonly>
      <input type="button" onclick="copy('whatsapp')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
      <label for="profissao" style="margin-left:15px">Profissão</label>
      <input type="text" value="<?=utf8_encode($aluno['profissao'])?>" style="margin-left:15px" class="form-control" id="profissao" name="profissao" readonly>
      <input type="button" onclick="copy('profissao')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
    </div>
    <div class="row form-inline mb-2">
      <label for="email">Email</label>
      <input type="text" value="<?=utf8_encode($aluno['email'])?>" value="<?=utf8_encode($aluno['email'])?>"style="margin-left:15px;" class="form-control" id="email" name="email" readonly>
      <input type="button" onclick="copy('email')" class="btn-clipboard" style="margin-left:2px" title="" src="Images/copy.png" data-original-title="Copy to clipboard"></button>
    </div>
    </div>
    </form>