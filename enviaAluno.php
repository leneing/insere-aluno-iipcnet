<?php
include "conectasql.php";
session_start();


$date = date_create_from_format('d/m/Y', $_POST["datanasc"]);
$date->getTimestamp();
$datanasc = $date->format('Y-m-d H:i:s');

$nome = utf8_decode($_POST["nome"]);


$sql = $conexao->prepare("UPDATE aluno_sem_iipcnet set codigo_iipcnet = (?) WHERE rg = (?)");  
$sql ->bind_param("is",$_POST["cod_iipcnet"],$_POST["rg"]); 
$res = $sql->execute();

$sql = $conexao->prepare("INSERT into aluno VALUES(?,?,?,?,?,now(),?,null,?,?,?,?,?)");  
$sql ->bind_param("issssssssss",$_POST["cod_iipcnet"],$_POST["cidade"],$_POST["email"],
$nome,$_POST["uf"],$datanasc,$_POST["celular"],$_POST["fixo"],$_POST["whatsapp"],$_POST["rg"],$_POST["cpf"]); 
$res = $sql->execute();


$sql = $conexao->prepare("UPDATE evento_interesse e set e.aluno_CODIGO = (?) WHERE e.aluno_CODIGO = (?) and e.id_questionario_pesquisa IS NOT NULL");
$sql ->bind_param("ss",$_POST["cod_iipcnet"],$_POST["rg"]); 
$res = $sql->execute();


$sql = $conexao->prepare("UPDATE tema_interesse t set t.aluno_CODIGO = (?) WHERE t.aluno_CODIGO = (?) and t.id_questionario_pesquisa IS NOT NULL");
$sql ->bind_param("ss",$_POST["cod_iipcnet"],$_POST["rg"]); 
$res = $sql->execute();

$sql->close();